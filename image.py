from tkinter import ttk, filedialog, Frame, Button, Tk, Canvas, NW, BOTH, Label, Menu
from PIL import Image, ImageTk, ImageFilter 
import numpy as np
from transforms import *



class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        master.title("ImPy")
        screen_width = root.winfo_screenwidth()
        screen_height = root.winfo_screenheight()
        master.minsize(width=640, height=400)
        master.maxsize(width=screen_width, height=screen_height)
        # master.bind("<Configure>", self.update_image_display)  # Se cicla
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.menu_bar = Menu(self)

        self.file_menu = Menu(self.menu_bar)
        self.file_menu.add_command(label="Open", command=self.open_image)
        self.file_menu.add_command(label="Save", command=self.save_image)
        self.file_menu.add_command(label="Exit", command=self.master.destroy)

        self.native_menu = Menu(self.menu_bar)
        self.native_menu.add_command(label="Blur", command=self.apply_native_filter(ImageFilter.BLUR))
        self.native_menu.add_command(label="Contour", command=self.apply_native_filter(ImageFilter.CONTOUR))
        self.native_menu.add_command(label="Detail", command=self.apply_native_filter(ImageFilter.DETAIL))
        self.native_menu.add_command(label="Edges", command=self.apply_native_filter(ImageFilter.EDGE_ENHANCE))
        self.native_menu.add_command(label="Edges+", command=self.apply_native_filter(ImageFilter.EDGE_ENHANCE_MORE))
        self.native_menu.add_command(label="Emboss", command=self.apply_native_filter(ImageFilter.EMBOSS))
        self.native_menu.add_command(label="Find Edges", command=self.apply_native_filter(ImageFilter.FIND_EDGES))
        self.native_menu.add_command(label="Sharpen", command=self.apply_native_filter(ImageFilter.SHARPEN))
        self.native_menu.add_command(label="Smooth", command=self.apply_native_filter(ImageFilter.SMOOTH))
        self.native_menu.add_command(label="Smooth+", command=self.apply_native_filter(ImageFilter.SMOOTH_MORE))

        self.filters_menu = Menu(self.menu_bar)
        self.filters_menu.add_command(label="High contrast", command=self.apply_custom_filter(high_contrast))
        self.filters_menu.add_command(label="Gray", command=self.apply_custom_filter(gray_scale))
        self.filters_menu.add_command(label="B&W", command=self.apply_custom_filter(black_and_white))
        self.filters_menu.add_command(label="Emboss U", command=self.apply_custom_filter(up_emboss))
        self.filters_menu.add_command(label="Emboss UR", command=self.apply_custom_filter(up_right_emboss))
        self.filters_menu.add_command(label="Emboss R", command=self.apply_custom_filter(right_emboss))
        self.filters_menu.add_command(label="Emboss DR", command=self.apply_custom_filter(down_right_emboss))
        self.filters_menu.add_command(label="Emboss D", command=self.apply_custom_filter(down_emboss))
        self.filters_menu.add_command(label="Emboss DL", command=self.apply_custom_filter(down_left_emboss))
        self.filters_menu.add_command(label="Emboss L", command=self.apply_custom_filter(left_emboss))
        self.filters_menu.add_command(label="Emboss UL", command=self.apply_custom_filter(up_left_emboss))

        self.menu_bar.add_cascade(label="File", menu=self.file_menu)
        self.menu_bar.add_cascade(label="Native", menu=self.native_menu)
        self.menu_bar.add_cascade(label="Filters", menu=self.filters_menu)

        self.master.config(menu=self.menu_bar)

        self.img_label = Label(self, bg="black")
        self.img_label.pack(side="bottom", fill="both",
                            anchor="nw", expand=True)
        self.current_img = None
    def apply_custom_filter(self, filter):
        def apply_custom_filter_to_current_image():
            image_array = np.asarray(self.current_img)
            new_array = filter(image_array)
            self.working_img = Image.fromarray(new_array)
            self.update_image_display(None)
        return apply_custom_filter_to_current_image

    def apply_native_filter(self, filter):
        def apply_filter_to_current_image():
            self.working_img = self.current_img.filter(filter)
            self.update_image_display(None)
        return apply_filter_to_current_image

    def update_image_display(self, event):
        if self.working_img == None:
            return None
        else:
            new_width = self.master.winfo_width()
            new_height = self.master.winfo_height()
            self.resized_image = self.working_img.resize(
                (new_width, new_height), Image.ANTIALIAS)
            image = ImageTk.PhotoImage(self.resized_image)
            self.img_label.configure(image=image)
            self.img_label.image = image

    def open_image(self):
        filename: str = filedialog.askopenfilename(initialdir=".")
        self.current_img = Image.open(filename)
        self.working_img = self.current_img.copy()

        self.update_image_display(None)

    def save_image(self):
        filename: str = filedialog.asksaveasfilename(initialdir=".")
        self.working_img.save(filename)


root = Tk()
app = Application(master=root)
app.mainloop()
