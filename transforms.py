import numpy as np
from scipy import signal


def custom_contrast(image_pixels, l_thresh=[64, 128, 192], l_maxval=[64, 128, 192]):
    new_image = np.empty_like(image_pixels)
    for i, thresh, maxval in zip(range(3), l_thresh, l_maxval):
        new_image[:, :, i] = (image_pixels[:, :, i] > thresh) * maxval
    return new_image

def high_contrast(image_pixels, thresh=127, max_value=255):
    return custom_contrast(image_pixels, [thresh] * 3, [max_value] * 3)

def gray_scale(image_pixels):
    new_image = np.average(image_pixels, axis=2)
    new_image = np.reshape(new_image, new_image.shape + (1,))
    new_image = np.repeat(new_image, 3, axis=2)
    new_image = new_image.astype(np.uint8)
    return new_image

def black_and_white(image_pixels):
    return high_contrast(gray_scale(image_pixels))

def convolution(image_pixels, mask):
    new_image = signal.convolve(image_pixels, mask, mode="same")
    return new_image.clip(0, 255).astype(np.uint8)

def simple_convolution(image_pixels, simple_mask):
    mask = np.reshape(np.array(simple_mask), (3,3,1))
    mask = np.repeat(mask, 3, axis=2)
    new_image = convolution(image_pixels, mask)
    return new_image

def up_emboss(image_pixels):
    return simple_convolution(image_pixels, [[0,1,0], [0,0,0], [0,-1,0]])

def up_right_emboss(image_pixels):
    return simple_convolution(image_pixels, [[0,0,1], [0,0,0], [-1,0,0]])

def right_emboss(image_pixels):
    return simple_convolution(image_pixels, [[0,0,0], [-1,0,1], [0,0,0]])

def down_right_emboss(image_pixels):
    return simple_convolution(image_pixels, [[-1,0,0], [0,0,0], [0,0,1]])

def down_emboss(image_pixels):
    return simple_convolution(image_pixels, [[0,-1,0], [0,0,0], [0,1,0]])

def down_left_emboss(image_pixels):
    return simple_convolution(image_pixels, [[0,0,-1], [0,0,0], [1,0,0]])

def left_emboss(image_pixels):
    return simple_convolution(image_pixels, [[0,0,0], [1,0,-1], [0,0,0]])

def up_left_emboss(image_pixels):
    return simple_convolution(image_pixels, [[1,0,0], [0,0,0], [0,0,-1]])
